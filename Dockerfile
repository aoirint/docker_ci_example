# syntax=docker/dockerfile:1.4
FROM ubuntu:22.04

RUN <<EOF
    set -eu

    apt-get update
    apt-get install -y \
        gosu

    apt-get clean
    rm -rf /var/lib/apt/lists/*
EOF
